odoo.define('js_pago_movil', function (require) {
    "use strict";

    var PopupWidget = require('point_of_sale.popups');
    var gui = require('point_of_sale.gui');
    var screens = require('point_of_sale.screens');

    var PagoMovilPopup = PopupWidget.extend({ // Popup de pago movil
        template: 'PagoMovilPopup',
        events: {
            'click .button.cancel': 'click_cancel',
            'click .button.confirm': 'click_confirm',
        },

        show: function (options) {
            var self = this;
            var client = this.pos.get_order().get_client();
            var pago_en_cache = this.pos.get_order().pago_movil
            if(pago_en_cache == null){
                var id_orden_act = this.pos.get_order().uid
                var ordenes = this.pos.db.get_unpaid_orders()
                for(let i = 0; i < ordenes.length; i++){
                    if(ordenes[i].uid === id_orden_act)
                    pago_en_cache = ordenes[i].pago_movil
                }
            }
            this._super(options);
            this.renderElement();
            if(pago_en_cache){ // Se revisa si hay un pago en cache asociado a esta orden y se muestran esos datos
                console.log('Hay un pago en cache', pago_en_cache)
                this.$('.name').attr('value', pago_en_cache.cliente); 
                this.$('.cedula').attr('value', pago_en_cache.cedula);
                this.$('.monto').attr('value', pago_en_cache.monto);
                this.$('.banco').attr('value', pago_en_cache.banco);
                this.$('.phone').attr('value', pago_en_cache.phone);
            }
            else{
                this.$('.name').attr('value', client.name); //Se toman los datos del cliente registrado
                this.$('.phone').attr('value', client.phone); // en el sistema
                this.$('.monto').attr('value', this.pos.get_order().get_change());
            }
        },
        click_cancel: function (event) {
            this.gui.close_popup();
        },
        check_required_inputs: function() { //Función que verifica que el usuario introdujo todos los datos
            var result = true
            this.$('.required').each(function () {
                if ($(this).val() == "") {
                    result = false;
                }
            });
            return result;
        },
        check_phone: function(phone) {
            //var regex = /04(12|14|16|24)-[0-9]{7}/
            var regex = new RegExp('04(12|14|16|24)-[0-9]{7}')
            return regex.test(phone)
        },

        check_cedula: function(cedula) {
            //var regex = new RegExp('(V|J|E)\d+')
            var regex = new RegExp('(V|J|E)[0-9]+')
            return regex.test(cedula)
        },

        click_confirm: function (event) {
            if(this.check_required_inputs()){
                if(this.check_phone(this.$('.phone').val())){
                    if(this.check_cedula(this.$('.cedula').val())){
                        var pago = {
                            order_id: this.pos.get_order().uid,
                            cliente: this.$('.name').val(),
                            cedula: this.$('.cedula').val(),
                            phone: this.$('.phone').val(),
                            banco: this.$('.banco').val(),
                            monto: this.$('.monto').val()
                        };
                        this.pos.get_order().guardar_pago(pago)
                        this.gui.close_popup();
                    }
                    else{
                        alert("Introduzca un RIF o una cedula valida")
                    }
                }
                else{
                    alert("Introduzca un numero de telefono valido")
                }
            }
            else{
                alert("Debe introducir todos los datos necesarios")
            }
        }
    });

    gui.define_popup({ name: 'pago-movil-popup', widget: PagoMovilPopup });

    screens.PaymentScreenWidget.include({ // Se extiende la vista de pago
        render_paymentlines: function () { // Función que renderiza el boton
            var self = this;
            this._super();
            var order = this.pos.get_order();
            var change = order.get_change();
            this.$('.js_pago_movil').toggle(change > 0)// si el vuelto es mayor a 0
            this.$('.js_pago_movil').unbind().click(function () {
                self.gui.show_popup('pago-movil-popup', {})
            });
        },
        init: function (parent, options) { // Funciones para desbloquear el teclado en la pantalla de pago
            var self = this;
            this._super(parent, options);
            //Overide methods
            this.keyboard_keydown_handler = function (event) {
                if (event.keyCode === 8 || event.keyCode === 46) { // Backspace and Delete
                    //event.preventDefault();
                    self.keyboard_handler(event);
                }
            };

            this.keyboard_handler = function (event) {
                var key = '';

                if (event.type === "keypress") {
                    if (event.keyCode === 13) { // Enter
                        self.validate_order();
                    } else if (event.keyCode === 190 || // Dot
                        event.keyCode === 110 ||  // Decimal point (numpad)
                        event.keyCode === 188 ||  // Comma
                        event.keyCode === 46) {  // Numpad dot
                        key = self.decimal_point;
                    } else if (event.keyCode >= 48 && event.keyCode <= 57) { // Numbers
                        key = '' + (event.keyCode - 48);
                    } else if (event.keyCode === 45) { // Minus
                        key = '-';
                    } else if (event.keyCode === 43) { // Plus
                        key = '+';
                    } else {
                        return;
                    }
                } else { // keyup/keydown
                    if (event.keyCode === 46) { // Delete
                        key = 'CLEAR';
                    } else if (event.keyCode === 8) { // Backspace
                        key = 'BACKSPACE';
                    }
                }
            };
        }
    })
});
