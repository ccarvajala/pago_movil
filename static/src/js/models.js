odoo.define('pago_movil.models', function (require) {
    "use strict";

    var models = require('point_of_sale.models');

    var exports = {}
    exports['Order'] = models.Order;

    var OrderSuper = models.Order.prototype;

    models.load_fields('pos.order', ['pago_movil_ids'])
    models.load_fields('pos.config', ['pago_movil_id'])

    models.Order = models.Order.extend({ // Extension del modelo de orden del tpv
        initialize: function () {
            OrderSuper.initialize.apply(this, arguments);
            if (this.pago_movil) { // carga los datos del pago en models.Order
                this.pago_movil = this.pos.pago_movil;
            }
        },
        guardar_pago: function(pago) { // Guarda los datos del pago en el localStorage
            this.pago_movil = pago;
            let pagos = this.pos.db.load('pago_movil',{})
            pagos[pago.order_id] = pago
            console.log('antes de guardar:', pagos)
            this.pos.db.save('pago_movil', pagos)
        },

        export_as_JSON: function () {
            var data = OrderSuper.export_as_JSON.apply(this, arguments);
            data.pago_movil = this.pago_movil;
            return data
        },

        init_from_JSON: function (json) {
            this.pago_movil = json.pago_movil;
            OrderSuper.init_from_JSON.call(this, json);
        },
    });

    models.load_models([{ // Carga del modelo de pago movil en el frontend
        model: 'pago_movil.pago_movil',
        label: 'pago_movil',
        fields: [],
        domain: function(self){ return [['id', '=', self.config.pago_movil_id[0]]];},
        loaded: function (self) {
            var pagos = self.db.load('pago_movil', {})
            console.log('despues de cargar', pagos)
            if (pagos && !(_.isEmpty(pagos))){ // Si hay pagos
                var keys_pagos = Object.keys(pagos)
                var orders = self.db.get_unpaid_orders() // se toman las ordenes sin pagar
                console.log('Orders:', orders)
                console.log('Orders Length', orders.length)
            
                orders.forEach( order => {
                    console.log('entra en el forEach')
                    keys_pagos.forEach(key =>{
                        console.log('recorre los pagos')
                        if(order.uid == pagos[key].order_id){ // Por cada orden se recorren todos los pagos
                            console.log('Se cumple y se asigna')// Y se verifica si tienen el mismo id
                            order.pago_movil = pagos[key] // se le asigna el pago movil a la orden correspondiente
                        }
                    })
                });
            }
            pagos = {}
            self.db.save('pago_movil', pagos) //Se vacían las ordenes 
        }
    }])
})