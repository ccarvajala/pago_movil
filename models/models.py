# -*- coding: utf-8 -*-

from odoo import models, fields, api


class extension_orden(models.Model):
    _inherit = 'pos.order'

    pago_movil_ids = fields.One2many(
        'pago_movil.pago_movil', 'pos_orden_id', string='Ids pago Movil')

    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(extension_orden, self)._order_fields(ui_order)
        order_fields['pago_movil_ids'] = ui_order.get('pago_movil_ids')

        return order_fields

    @api.model
    def _process_order(self, pos_order):
        order = super(extension_orden, self)._process_order(pos_order)
        if 'pago_movil' in pos_order:
            pago = self.env['pago_movil.pago_movil']
            pago.create({
                'cliente': pos_order['partner_id'],
                'phone': pos_order['pago_movil']['phone'],
                'banco': pos_order['pago_movil']['banco'],
                'cedula': pos_order['pago_movil']['cedula'],
                'monto': pos_order['pago_movil']['monto'],
                'vendedor' : pos_order['user_id'],
                'pos_orden_id' : pos_order['name']
            })

        return order


class extension_config(models.Model):
    _inherit = 'pos.config'

    pago_movil_id = fields.Many2one(
        'pago_movil.pago_movil', string='ID pago Movil')


class pago_movil(models.Model):
    _name = 'pago_movil.pago_movil'

    pos_orden_id = fields.Char(string='Numero de Orden')

    estado_pago = fields.Selection([
        ('B', 'Borrador'),
        ('E', 'En Espera'),
        ('P', 'En Proceso'),
        ('R', 'Realizado'),
        ('C', 'Cancelado')
    ], string='Estado', default='B', readonly=True, index=True, track_visibility='onchange', copy=False)

    cliente = fields.Many2one('res.partner', string='Cliente', store=True)
    phone = fields.Char(string="Num Tlf", store=True)
    # Cedula y tlf se traen del sistema o se introducen?
    cedula = fields.Char(string='Cedula de identidad')

    banco = fields.Selection([
        ('0102', '0102 - Banco de Venezuela'),
        ('0104', '0104 - Venezolano de Credito'),
        ('0105', '0105 - Mercantil'),
        ('0108', '0108 - Provincial'),
        ('0114', '0114 - Bancaribe'),
        ('0115', '0115 - Exterior'),
        ('0116', '0116 - BOD'),
        ('0128', '0128 - Banco Caroni'),
        ('0134', '0134 - Banesco'),
        ('0138', '0138 - Banco Plaza'),
        ('0151', '0151 - BFC'),
        ('0156', '0156 - 100% Banco'),
        ('0157', '0157 - Banco Del Sur'),
        ('0163', '0163 - Banco Del Tesoro'),
        ('0166', '0166 - Banco Agricola de Venezuela'),
        ('0168', '0168 - Bancrecer'),
        ('0169', '0169 - Mi Banco'),
        ('0172', '0172 - Bancamiga'),
        ('0174', '0174 - Banplus'),
        ('0175', '0175 - Bicentenario del Pueblo'),
        ('0177', '0177 - Banfanb'),
        ('0191', '0191 - BNC')
    ], string='Banco', index=True, track_visibility='onchange', copy=False)

    monto = fields.Float(string='monto a pagar', store=True)
    pago_auto = fields.Boolean(string='Pago Automático')
    responsable_pago = fields.Many2one('res.users', string='Responsable Pago')
    vendedor = fields.Many2one('res.users', string='Vendedor', store=True)

    def espera_pago(self):
        self.estado_pago = 'E'

    def proceso_pago(self):
        self.estado_pago = 'P'

    def realizado_pago(self):
        self.estado_pago = 'R'

    def cancelado_pago(self):
        self.estado_pago = 'C'

    def borrador_pago(self):
        self.estado_pago = 'B'
